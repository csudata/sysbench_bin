pathtest = string.match(test, "(.*/)") or ""

dofile(pathtest .. "common.lua")

function thread_init(thread_id)
   set_vars()
   local table_name
   table_name = "sbtest".. sb_rand_uniform(1, oltp_tables_count)
   stmt = db_prepare("SELECT pad FROM ".. table_name .." WHERE id=?")
   params = {}
   params[1] = 1
   db_bind_param(stmt, params)
end

function event(thread_id)
	params[1] = sb_rand(1, oltp_table_size) 
    rs = db_execute(stmt)
    db_store_results(rs)
    db_free_results(rs)
end

