#!/bin/bash

./sysbench --test=lua/select_prepare.lua \
   --db-driver=pgsql \
   --pgsql-host=127.0.0.1 \
   --pgsql-port=5432 \
   --pgsql-user=postgres \
   --pgsql-password=rootR00t \
   --pgsql-db=test02  \
   --oltp-tables-count=1 \
   --oltp-table-size=100000 \
   --num-threads=1 \
   --max-requests=100000000 \
   --max-time=300 \
   --report-interval=1 \
   $*
