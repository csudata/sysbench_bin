## sysbench_bin

此工程的d测试oracle可以会coredump，已经用asbench替代了，请关注： https://gitee.com/csudata/asbench

在asbench中release页面中下载已经编译好的测试工具。


为PostgreSQL、MySQL、Oracle编译好了sysbench，方便做对比测试。

如需交流：
* 加微信号：osdba0 
* 加QQ号：191421283

## 捐助

如果觉得对您有所帮助，请扫下方二维码打赏我们一杯咖啡:

![捐赠](img/donation.png)


## 使用方法


* Prepare命令
  - 用于生成测试数据
  - 命令行格式为sysbench <options> prepare
* Run命令
  - 运行测试的命令
  - 命令行格式为sysbench <options> run
* Cleanup命令
  - 清除测试数据
  - 命令行格式为sysbench <options> cleanup
 

oracle的测试命令：

```
./sysbench_ora --test=lua/oltp.lua \
    --oltp-table-name=sysbench \
    --oltp-table-size=10000000 \
    --oltp-tables-count=320 \
    --oracle-db=bcache \
    --oracle-user=sysbench \
    --oracle-password=sysbench \
    --max-time=7200 \
    --max-requests=0 \
    --num-threads=32 \
    --db-driver=oracle \
    --report-interval=1 \
    run
```

PostgreSQL的测试命令：

```
./sysbench_pg --test=lua/select_prepare.lua \
   --db-driver=pgsql \
   --pgsql-host=127.0.0.1 \
   --pgsql-port=5432 \
   --pgsql-user=postgres \
   --pgsql-password=rootR00t \
   --pgsql-db=test02  \
   --oltp-tables-count=1 \
   --oltp-table-size=100000 \
   --num-threads=1 \
   --max-requests=100000000 \
   --max-time=300 \
   --report-interval=1 \
   run
```



## 编译 


在github上放了原理的代码：

* https://github.com/osdba/sysbench2



```
./autogen.sh
./configure --with-oracle --without-mysql --without-drizzle

make ORA_LIBS="-L$ORACLE_HOME/lib -lclntsh" ORA_CFLAGS="-I$ORACLE_HOME/rdbms/demo -I$ORACLE_HOME/rdbms/public"
```

## 脚本的修改


oltp.lua脚本使用dofile包含了common.lua
dofile(pathtest .. "common.lua")
common.lua脚本主要负责生成测试数据

主要修改
* common.lua：原来的脚本没有支持Oracle数据库
* oltp.lua：原来的脚本PostgreSQL下不能正确运行测试
* update_index.lua=>update_index_with_commit.lua: 原先的脚本中每次操作没有commit语句，这样在测试的输出中tps值为零。
* update_index.lua => update_index_ora.lua
    原先的脚本测试Oracle时，Sysbench本身会占用大量的CPU，导致数据库本身的压力上不去。
    原因是Sysbench中原先的drv_oracle.c代码中访问Oracle的部分不够优化，每次执行时都会调用rc = OCIHandleAlloc(ora_env,...)和OCIHandleFree(stmt, OCI_HTYPE_STMT);而多线程下这两个函数的调用会导致了强烈的锁竞争。
    解决办法：使用绑定变量，但使用绑定变量也有一些问题，问题描述见下一页
    - Oracle中直接使用Sysbech提供的绑定变量函数db_prepare的问题。直接报错，不能正常运行。原因是代码有BUG，但可以绕过去。解决方法是另加一个无用的字符串类型的绑定变量
     ```
     function thread_init(thread_id)
       local table_name
       set_vars()
       table_name = "sbtest".. (thread_id+1)
       stmt = db_prepare("UPDATE ".. table_name .." SET k=k+1 WHERE id=to_number(:x) and 'a' = :y")
       params = {}
       params[1] = '444'
       params[2] = 'a'
       db_bind_param(stmt, params)
    end
    
    function event(thread_id)
       local table_name
       params[1] = string.format("%d", sb_rand(1, oltp_table_size))
       params[2] = 'a'
       db_execute(stmt)
       db_query('COMMIT')
    end
    ```
    上面增加了一个无用的`params[2]='a'`
    

## 增强

如果想在Oracle中造几百GB的数据，请参照：

[sysbench测试Oracle时写一个lua脚本调用sqlload快速造数据](http://blog.osdba.net/538.html)

这是通过C语言写的程序来快速生成测试数据库，同时调用sqlldr快速的把测试数据装载到Oracle中的方法。





